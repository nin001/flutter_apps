import 'package:quiz_app_1/question.dart';

class GetDSAQuestions {
  static List<Question> quesList = [];

  static List<Question> getList() {
    quesList = [];
    quesList.add(Question(
        ques:
            "1.  Which one of the following is an application of Stack Data Structure?",
        option1: "Managing function calls",
        option2: "The stock span problem",
        option3: "Arithmetic expression evaluation",
        option4: "All of the above",
        correctOption: 4));
    quesList.add(Question(
        ques:
            "2. Which one of the following is an application of Queue Data Structure?",
        option1: "When a resource is shared among multiple consumers.",
        option2:
            "When data is transferred asynchronously (data not necessarily received at same rate as sent) between two processes",
        option3:
            "When data is transferred synchronously (data not necessarily received at same rate as sent) between two processes",
        option4: "All of the above",
        correctOption: 4));
    quesList.add(Question(
        ques:
            "3. Which of the following sorting algorithms can be used to sort a random linked list with minimum time complexity?",
        option1: "Insertion Sort",
        option2: "Quick Sort",
        option3: "Bubble sort",
        option4: "MergeSort",
        correctOption: 4));
    quesList.add(Question(
        ques:
            "4. In the worst case, the number of comparisons needed to search a singly linked list of length n for a given element is",
        option1: "n",
        option2: "logn",
        option3: "n^2",
        option4: "n/2",
        correctOption: 1));
    quesList.add(Question(
        ques: "5. Trie is also known as ",
        option1: "Treap",
        option2: "Binomial tree",
        option3: "2-3 Tree",
        option4: "Digital Tree",
        correctOption: 4));

    return quesList;
  }
}

import 'package:quiz_app_1/question.dart';

class GetOSQuestions {
  static List<Question> quesList = [];

  static List<Question> getList() {
    quesList = [];
    quesList.add(Question(
        ques: "1. Which of the following are CPU scheduling algorithms?",
        option1: "Priority Queue",
        option2: "Round Robin",
        option3: "Shortest Job First",
        option4: "All of the above",
        correctOption: 4));
    quesList.add(Question(
        ques:
            "2. A process which is copied from main memory to secondary memory on the basis of requirement is known as",
        option1: "Demand Paging",
        option2: "Paging",
        option3: "Threads",
        option4: "Segmentation",
        correctOption: 1));
    quesList.add(Question(
        ques:
            "3. Which of the type of OS reads and reacts in terms of actual time?",
        option1: "Quick Sharing OS",
        option2: "Time Sharing OS",
        option3: "Real Time  OS",
        option4: "Batch OS",
        correctOption: 3));
    quesList.add(Question(
        ques:
            "4. A systematic procedure for moving the CPU to new process is known as",
        option1: "Synchronization",
        option2: "Deadlock",
        option3: "Starvation",
        option4: "Context switching",
        correctOption: 4));
    quesList.add(Question(
        ques: "5. UNIX is written in which language? ",
        option1: "C#",
        option2: "C++",
        option3: "C",
        option4: ".Net",
        correctOption: 3));

    return quesList;
  }
}

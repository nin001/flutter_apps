import 'package:flutter/material.dart';
import 'package:quiz_app_1/c.dart';
import 'package:quiz_app_1/dsa.dart';
import 'package:quiz_app_1/mobile_dev.dart';
import 'package:quiz_app_1/os.dart';
import 'package:quiz_app_1/question.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: QuizHomePage(),
    );
  }
}

class QuizHomePage extends StatefulWidget {
  const QuizHomePage({super.key});

  @override
  State<QuizHomePage> createState() => _HomepageState();
}

class _HomepageState extends State<QuizHomePage> {
  static int selectedQuiz = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Quiz App",
          style: TextStyle(
              fontSize: 25, fontWeight: FontWeight.w700, letterSpacing: 1.5),
        ),
        backgroundColor: const Color.fromARGB(255, 83, 223, 204),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 30,
          ),
          Container(
            height: 100,
            width: double.infinity,
            //color: Colors.greenAccent,
            padding: const EdgeInsets.all(3.5),
            child: ElevatedButton(
              onPressed: () {
                _HomepageState.selectedQuiz = 1;
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const QuizApp()));
              },
              child: Image.asset("images/flutter.png"),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Container(
            height: 100,
            width: double.infinity,
            //color: Colors.greenAccent,
            padding: const EdgeInsets.all(3.5),
            child: ElevatedButton(
              onPressed: () {
                _HomepageState.selectedQuiz = 2;
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const QuizApp()));
              },
              child: Image.asset("images/os.png"),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Container(
            height: 100,
            width: double.infinity,
            //color: Colors.greenAccent,
            padding: const EdgeInsets.all(3.5),
            child: ElevatedButton(
              onPressed: () {
                _HomepageState.selectedQuiz = 3;
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const QuizApp()));
              },
              child: Image.asset("images/c.png"),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Container(
            height: 100,
            width: double.infinity,
            //color: Colors.greenAccent,
            padding: const EdgeInsets.all(3.5),
            child: ElevatedButton(
              onPressed: () {
                _HomepageState.selectedQuiz = 4;
                Navigator.push(context,
                    MaterialPageRoute(builder: ((context) => const QuizApp())));
              },
              child: Image.asset("images/dsa.jpg"),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State<QuizApp> createState() => _QuizState();
}

class _QuizState extends State<QuizApp> {
  String? ques = "";
  String? option1 = "";
  String? option2 = "";
  String? option3 = "";
  String? option4 = "";

  List<Question> list = [];
  int index = 0;
  int queNo = 1;

  int optionChoosen = -1;
  int result = 0;

  MaterialStatePropertyAll<Color?> check(int option) {
    if (optionChoosen != -1) {
      if (option == list[index].correctOption) {
        result++;
        return const MaterialStatePropertyAll(Colors.green);
      } else if (option == optionChoosen) {
        result--;
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(Colors.white);
      }
    } else {
      return const MaterialStatePropertyAll(Colors.white);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_HomepageState.selectedQuiz == 1) {
      list = GetMobileDevQuestions.getList();
    } else if (_HomepageState.selectedQuiz == 2) {
      list = GetOSQuestions.getList();
    } else if (_HomepageState.selectedQuiz == 3) {
      list = GetCQuestions.getList();
    } else {
      list = GetDSAQuestions.getList();
    }
    ques = list[index].ques;
    option1 = list[index].option1;
    option2 = list[index].option2;
    option3 = list[index].option3;
    option4 = list[index].option4;

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Tech Quiz App",
          style: TextStyle(
              fontSize: 25, fontWeight: FontWeight.w700, letterSpacing: 1.5),
        ),
        backgroundColor: const Color.fromARGB(255, 83, 223, 204),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.greenAccent,
        child: Container(
          height: 400,
          width: 400,
          color: Colors.greenAccent,
          padding:
              const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 0),
          child: Column(children: [
            Text("$ques",
                style: const TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 1.0,
                ),
                textAlign: TextAlign.center),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 40,
              width: 400,
              color: Colors.greenAccent,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    optionChoosen = 1;
                  });
                },
                style: ButtonStyle(backgroundColor: check(1)),
                child: Text(
                  "$option1",
                  style: const TextStyle(
                      letterSpacing: 1.0, fontWeight: FontWeight.w500),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 40,
              width: 400,
              color: Colors.greenAccent,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    optionChoosen = 2;
                  });
                },
                style: ButtonStyle(backgroundColor: check(2)),
                child: Text(
                  "$option2",
                  style: const TextStyle(
                      letterSpacing: 1.0, fontWeight: FontWeight.w500),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 40,
              width: 400,
              color: Colors.greenAccent,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    optionChoosen = 3;
                  });
                },
                style: ButtonStyle(backgroundColor: check(3)),
                child: Text(
                  "$option3",
                  style: const TextStyle(
                      letterSpacing: 1.0, fontWeight: FontWeight.w500),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 40,
              width: 400,
              color: Colors.greenAccent,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    optionChoosen = 4;
                  });
                },
                style: ButtonStyle(backgroundColor: check(4)),
                child: Text(
                  "$option4",
                  style: const TextStyle(
                      letterSpacing: 1.0, fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ]),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print(result);
          if (queNo == list.length) {
            _Congratulations.score = result;
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const CongratulationsScreen(),
              ),
            );
          }
          setState(() {
            if (index < list.length - 1) {
              queNo++;
              index++;
            }
            optionChoosen = -1;
          });
        },
        child: const Icon(Icons.next_plan),
      ),
    );
  }
}

class CongratulationsScreen extends StatefulWidget {
  const CongratulationsScreen({super.key});

  @override
  State<CongratulationsScreen> createState() => _Congratulations();
}

class _Congratulations extends State<CongratulationsScreen> {
  static int score = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Congratulations!',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20),
            const Text(
              'Your Score:',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            Text(
              '${_Congratulations.score} / 5',
              style: const TextStyle(
                fontSize: 36,
                fontWeight: FontWeight.bold,
                color: Colors.green,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.popUntil(context, (route) => route.isFirst);
                },
                child: const Text(
                  'Home',
                  style: TextStyle(
                    fontSize: 36,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ))
          ],
        ),
      ),
    );
  }
}

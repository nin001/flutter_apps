class Question {
  String? ques;
  String? option1;
  String? option2;
  String? option3;
  String? option4;

  int? correctOption;

  Question(
      {required this.ques,
      required this.option1,
      required this.option2,
      required this.option3,
      required this.option4,
      required this.correctOption});
}

import 'package:quiz_app_1/question.dart';

class GetMobileDevQuestions {
  static List<Question> quesList = [];

  static List<Question> getList() {
    quesList = [];
    quesList.add(Question(
        ques: "1.  What was the fastest growing web browser in 2020?",
        option1: "Microsoft Edge",
        option2: "Firefox",
        option3: "DuckDuckGo",
        option4: "Internet Explorer",
        correctOption: 2));
    quesList.add(Question(
        ques: "2. Who created Flutter?",
        option1: "Facebook",
        option2: "Adobe",
        option3: "Google",
        option4: "Microsoft",
        correctOption: 3));
    quesList.add(Question(
        ques: "3. Which programming language is used by Flutter",
        option1: "Ruby",
        option2: "Dart",
        option3: "C++",
        option4: "Kotlin",
        correctOption: 2));
    quesList.add(Question(
        ques: "4. Who created Dart programming language?",
        option1: "Lars Bak and Kasper Lund",
        option2: "Brendan Eich",
        option3: "Bjarne Stroustrup",
        option4: "Jeremy Ashkenas",
        correctOption: 1));
    quesList.add(Question(
        ques: "5. Is Flutter for Web and Desktop available in stable version?",
        option1: "Yes",
        option2: "No",
        option3: "Maybe",
        option4: "None of the above",
        correctOption: 2));

    return quesList;
  }
}

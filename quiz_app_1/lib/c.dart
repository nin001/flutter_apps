import 'package:quiz_app_1/question.dart';

class GetCQuestions {
  static List<Question> quesList = [];

  static List<Question> getList() {
    quesList = [];
    quesList.add(Question(
        ques: "1. Who is the father of C language?",
        option1: "Steve Jobs",
        option2: "James Gosling",
        option3: "Dennis Ritchie",
        option4: "Rasmus Lerdor",
        correctOption: 3));
    quesList.add(Question(
        ques: "2. Which of the following is not a valid C variable name?",
        option1: "int number;",
        option2: "float rate;",
        option3: "int variable_count;",
        option4: "int -main;",
        correctOption: 4));
    quesList.add(Question(
        ques: "3. All keywords in C are in",
        option1: "LowerCase",
        option2: "UpperCase",
        option3: "CamelCase",
        option4: "None of the above",
        correctOption: 1));
    quesList.add(Question(
        ques: "4. Which of the following is true for variable names in C?",
        option1:
            "They can contain alphanumeric characters as well as special characters",
        option2:
            "It is not an error to declare a variable to be one of the keywords(like goto, static)",
        option3: "Variable names cannot start with a digit",
        option4: "Variable can be of any length",
        correctOption: 3));
    quesList.add(Question(
        ques: "5. Which of the following cannot be a variable name in C? ",
        option1: "volatile",
        option2: "true",
        option3: "friend",
        option4: "export",
        correctOption: 1));

    return quesList;
  }
}

import 'package:flutter/material.dart';
import 'package:uniconnect1/loginscreen.dart';
import 'package:uniconnect1/projectmodel.dart';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

dynamic uniDB;
List<UniModal> projectList = [];
List<UniModal> domainSpecificList =
    []; // List for domain specific projects (for domain page)

List<UniModal> aiml = [];
List<UniModal> flutter = [];
List<UniModal> prog = [];
List<UniModal> web = [];

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  uniDB = await openDatabase(
    join(await getDatabasesPath(), 'UniDB.db'),
    version: 1,
    onCreate: (db, version) {
      db.execute('''CREATE TABLE projects(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        projectTitle TEXT,
        description TEXT,
        college TEXT,
        gitLink TEXT,
        domain TEXT,
        pic1 BLOB,
        pic2 BLOB,
        pic3 BLOB,
        pic4 BLOB
      )''');
      db.execute('''CREATE TABLE user_info(
        username TEXT PRIMARY KEY,
        password TEXT
      )''');
    },
  );

  String path = join(await getDatabasesPath(), 'UniDB.db');
  if (await databaseExists(path)) {
    projectList = await getAllProjects();
  }
  // runApp(const MainApp());

  runApp(const MyApp());
}

// Database code below
// DATA RETRIEVAL FUNCTIONS
Future<List<UniModal>> getAllProjects() async {
  final Database localDB = await uniDB;

  List<Map<String, dynamic>> list = await localDB.query('projects');

  return List.generate(list.length, (index) {
    return UniModal(
      id: list[index]['id'],
      projectTitle: list[index]['projectTitle'],
      description: list[index]['description'],
      gitLink: list[index]['gitLink'],
      domain: list[index]['domain'].toString(),
      college: list[index]['college'],
      pic1: list[index]['pic1'],
      pic2: list[index]['pic2'],
      pic3: list[index]['pic3'],
      pic4: list[index]['pic4'],
    );
  });
}

// DATA RETRIEVAL FUNCTIONS
Future<List<UniModal>> getDomainSpecificProjects(String domain) async {
  final Database localDB = await uniDB;

  List<Map<String, dynamic>> list =
      await localDB.query('projects', where: 'domain = ?', whereArgs: [domain]);

  return List.generate(list.length, (index) {
    return UniModal(
      id: list[index]['id'],
      projectTitle: list[index]['projectTitle'],
      description: list[index]['description'],
      gitLink: list[index]['gitLink'],
      domain: list[index]['domain'],
      college: list[index]['college'],
      pic1: list[index]['pic1'],
      pic2: list[index]['pic2'],
      pic3: list[index]['pic3'],
      pic4: list[index]['pic4'],
    );
  });
}

// INSERT
Future<void> insertProject(UniModal project) async {
  final Database localDB = await uniDB;

  await localDB.insert('projects', project.getMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);

  projectList = await getAllProjects();
}

// Delete Item
Future<void> deleteProject(UniModal obj) async {
  final Database localDB = await uniDB;

  await localDB.delete('projects', where: 'id = ?', whereArgs: [obj.id]);

  projectList = await getAllProjects();
}

// update
Future<void> updateProject(UniModal obj) async {
  final Database localDB = await uniDB;

  await localDB
      .update('projects', obj.getMap(), where: 'id = ?', whereArgs: [obj.id]);

  projectList = await getAllProjects();
}

// SignUp values (insert username and password into the database)
Future<void> insertUserInfo(String username, String password) async {
  final Database localDB = await uniDB;

  await localDB.insert(
      'user_info', {'username': username, 'password': password},
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<bool> checkUsername(String username) async {
  final Database localDB = await uniDB;

  List<Map<String, dynamic>> list = await localDB
      .query('user_info', where: 'username = ?', whereArgs: [username]);

  if (list.isEmpty) {
    return false;
  }
  return true;
}

Future<String> getPass(String username) async {
  final Database localDB = await uniDB;

  List<Map<String, dynamic>> list = await localDB
      .query('user_info', where: 'username = ?', whereArgs: [username]);

  if (list.isEmpty) {
    return "";
  }
  return list[0]["password"];
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: StartPage(),
    );
  }
}

class StartPage extends StatefulWidget {
  @override
  State createState() => _StartPageState();
}

class _StartPageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [
          Color.fromRGBO(197, 4, 98, 1),
          Color.fromRGBO(80, 3, 112, 1)
        ])),
        child: Padding(
          padding: const EdgeInsets.only(left: 20 , right:20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.only(left: 0, top: 100),
                child: Text(
                  "Welcome to",
                  style: TextStyle(
                      fontSize: 36.04,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(left: 1),
                child: Text(
                  "Uni-CONNECT",
                  style: TextStyle(
                      fontSize: 36.04,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
              ),
              const SizedBox(
                height: 250.54,
              ),
              Flexible(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.5),
                    borderRadius: BorderRadius.circular(16.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 15,
                        ),
                        const Text(
                          "Explore The Uniconnect APP",
                          style: TextStyle(
                            fontSize: 25.2,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        const Text(
                          "You can view projects of various domains",
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.yellow,
                            ),
                            onPressed: () {
                              setState(() {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const LoginPage()),
                                );
                              });
                            },
                            child: const Text(
                              "Get Started >>",
                              style: TextStyle(fontSize: 25),
                            ))
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

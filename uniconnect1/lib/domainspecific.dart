import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:uniconnect1/getstarted.dart';
import 'package:uniconnect1/screen1.dart';

class DomainList extends StatefulWidget {
  final String domainName;
  const DomainList({required this.domainName, super.key});

  @override
  State<DomainList> createState() => _DomainListState(domainName: domainName);
}

class _DomainListState extends State<DomainList> {
  void fillData() async {
    domainSpecificList = await getDomainSpecificProjects(domainName);
  }

  @override
  void initState() {
    // TODO: implement initState
    fillData();
    super.initState();
  }

  final String domainName;
  _DomainListState({required this.domainName});

  @override
  Widget build(BuildContext context) {
    log(domainName);
    log(domainSpecificList.length.toString());

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(197, 4, 98, 1),
                Color.fromRGBO(80, 3, 112, 1),
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 70),
              /*Padding(
                padding: const EdgeInsets.only(left: 20),
                child: GestureDetector(
                  onTap: () {
                    // Handle arrow back tap
                  },
                  child: const Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                ),
              ),*/
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.only(left: 38, right: 44),
                child: Text(
                  domainName,
                  style: const TextStyle(
                    fontSize: 32.61,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
              ),
              const SizedBox(height: 10),
              const SizedBox(height: 32),
              Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(38),
                    topRight: Radius.circular(38),
                  ),
                ),
                child: Padding(
                    padding: const EdgeInsets.all(30),
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: domainSpecificList.length,
                        itemBuilder: (context, index) {
                          String shortDescription =
                              "${domainSpecificList[index].description.substring(0, 50)}..";
                          return Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Screen(
                                              project:
                                                  domainSpecificList[index])));
                                },
                                child: SizedBox(
                                  height: 50,
                                  child: ListTile(
                                    title: Text(
                                        domainSpecificList[index].projectTitle),
                                    subtitle: Text(shortDescription),
                                    leading: Image.memory(
                                        domainSpecificList[index].pic1),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              )
                            ],
                          );
                        })),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class GalleryImage extends StatelessWidget {
  const GalleryImage({Key? key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(25)),
        color: Colors.amber,
      ),
      height: 200,
      width: 300,
      child: Image.network(
        "https://img.freepik.com/free-photo/beautiful-scenery-summit-mount-everest-covered-with-snow-white-clouds_181624-21317.jpg?size=626&ext=jpg&ga=GA1.1.1365615838.1705642289&semt=sph",
        fit: BoxFit.fill,
      ),
    );
  }
}

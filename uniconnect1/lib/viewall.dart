/*
import 'package:flutter/material.dart';
import 'package:uniconnect/screen1.dart';

List title = ["Domains", "Projects Gallery"];
List domains = ["AI/ML", "Flutter", "Programming Language", "Web-Development"];

class ViewAll extends StatefulWidget {
  @override
  State createState() => _ViewAllState();
}

class _ViewAllState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          const SizedBox(height: 47),
          // ignore: unnecessary_const
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              gradient: const LinearGradient(
                colors: [
                  Color.fromRGBO(197, 4, 98, 1),
                  Color.fromRGBO(80, 3, 112, 1),
                ],
              ),
            ),
            child: const Padding(
              padding: EdgeInsets.all(20.0),
              child: Text("All Domains",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  )),
            ),
          ),
          Expanded(
              child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(38),
                      topRight: Radius.circular(38),
                    ),
                  ),
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: domains.length,
                      itemBuilder: (context, innerIndex) {
                        final itemName = domains[innerIndex];
                        return Padding(
                          padding: const EdgeInsets.all(20),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const Screen(),
                                ),
                              );
                            },
                            child: Container(
                              height: 242,
                              width: 190,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                gradient: const LinearGradient(
                                  colors: [
                                    Color.fromRGBO(197, 4, 98, 1),
                                    Color.fromRGBO(80, 3, 112, 1),
                                  ],
                                ),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 22,
                                      right: 18,
                                    ),
                                    child: Text(
                                      itemName,
                                      style: const TextStyle(
                                        color: Color.fromRGBO(
                                          255,
                                          255,
                                          255,
                                          1,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 3),
                                  Expanded(
                                    child: Container(
                                      height: double.infinity,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: Image.asset(
                                        "assets/7010826_3255307.png",
                                        height: 160,
                                        width: 180,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      })))
        ]));
  }
}
*/
/*
//Changes for representing the proper names on the project titles
//as per the view all which i am visiting from main screen.
import 'package:flutter/material.dart';
import 'package:uniconnect/screen1.dart';

class ViewAll extends StatelessWidget {
  final List<String> itemList;
  final BuildContext context;

  const ViewAll({required this.itemList, required this.context});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 47),
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              gradient: const LinearGradient(
                colors: [
                  Color.fromRGBO(197, 4, 98, 1),
                  Color.fromRGBO(80, 3, 112, 1),
                ],
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                "All ${itemList == domains ? 'Domains' : 'Projects'}",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(38),
                  topRight: Radius.circular(38),
                ),
              ),
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: itemList.length,
                itemBuilder: (context, index) {
                  final item = itemList[index];
                  return _buildItem(item);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(String item) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            this.context,
            MaterialPageRoute(
              builder: (context) => const Screen(),
            ),
          );
        },
        child: Container(
          height: 242,
          width: 190,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            gradient: const LinearGradient(
              colors: [
                Color.fromRGBO(197, 4, 98, 1),
                Color.fromRGBO(80, 3, 112, 1),
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  top: 20,
                  left: 22,
                  right: 18,
                ),
                child: Text(
                  item,
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              const SizedBox(height: 3),
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Image.asset(
                    "assets/7010826_3255307.png",
                    height: 160,
                    width: 180,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
*/

/*
import 'package:flutter/material.dart';
import 'package:uniconnect/screen1.dart';
import 'package:uniconnect/main.dart';

class ViewAll extends StatelessWidget {
  final List<Domain> domainList;
  final BuildContext context; // Define context here

  const ViewAll(
      {Key? key, required this.domainList, required this.context}): super(key: key); // Receive context here

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 47),
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              gradient: const LinearGradient(
                colors: [
                  Color.fromRGBO(197, 4, 98, 1),
                  Color.fromRGBO(80, 3, 112, 1),
                ],
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                "All ${itemList == domains ? 'Domains' : 'Projects'}",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(38),
                  topRight: Radius.circular(38),
                ),
              ),
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: itemList.length,
                itemBuilder: (context, index) {
                  final item = itemList[index];
                  return _buildItem(item);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(String item) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context, // Use context from constructor
            MaterialPageRoute(
              builder: (context) => Screen(),
            ),
          );
        },
        child: Container(
          height: 242,
          width: 190,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            gradient: const LinearGradient(
              colors: [
                Color.fromRGBO(197, 4, 98, 1),
                Color.fromRGBO(80, 3, 112, 1),
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  top: 20,
                  left: 22,
                  right: 18,
                ),
                child: Text(
                  item,
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              const SizedBox(height: 3),
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Image.asset(
                    "assets/7010826_3255307.png",
                    height: 160,
                    width: 180,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
*/

import 'package:flutter/material.dart';
import 'package:uniconnect1/getstarted.dart';
import 'package:uniconnect1/screen1.dart'; // Import the Domain class

class ViewAll extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(197, 4, 98, 1),
                Color.fromRGBO(80, 3, 112, 1),
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 70),
              /*Padding(
                padding: const EdgeInsets.only(left: 20),
                child: GestureDetector(
                  onTap: () {
                    // Handle arrow back tap
                  },
                  child: const Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                ),
              ),*/
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.only(left: 38, right: 44),
                child: Text(
                  "All Projects",
                  style: const TextStyle(
                    fontSize: 32.61,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
              ),
              const SizedBox(height: 10),
              const SizedBox(height: 32),
              Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(38),
                    topRight: Radius.circular(38),
                  ),
                ),
                child: Padding(
                    padding: const EdgeInsets.all(30),
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: projectList.length,
                        itemBuilder: (context, index) {
                          String shortDescription =
                              "${projectList[index].description.substring(0, 50)}..";
                          return Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Screen(
                                              project: projectList[index])));
                                },
                                child: SizedBox(
                                  height: 50,
                                  child: ListTile(
                                    title:
                                        Text(projectList[index].projectTitle),
                                    subtitle: Text(shortDescription),
                                    leading:
                                        Image.memory(projectList[index].pic1),
                                  ),
                                ),
                              ),
                              const SizedBox(height: 20)
                            ],
                          );
                        })),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

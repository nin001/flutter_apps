//Last changes code module where all the viewall list contains proper names as per the Listview Builders.

import 'package:flutter/material.dart';
import 'package:uniconnect1/domainspecific.dart';
import 'package:uniconnect1/getstarted.dart';
import 'package:uniconnect1/viewall.dart';

//Model class for Domains
class Domain {
  final String name;
  final String imageUrl;
  final Color backgroundColor;

  Domain(
      {required this.name,
      required this.imageUrl,
      required this.backgroundColor});
}

//Model class for projects
class Project {
  final String name;
  final String imageUrl;
  final Color backgroundColor;

  Project(
      {required this.name,
      required this.imageUrl,
      required this.backgroundColor});
}

/*
List<Domain> domains = [
  Domain(
      name: 'AI/ML',
      imageUrl: "assets/7010826_3255307.png",
      backgroundColor: Colors.teal.shade400),
  Domain(
      name: "Flutter",
      imageUrl: "assets/7010826_3255307.png",
      backgroundColor: Colors.blue.shade400),
  Domain(
      name: "Programming Language",
      imageUrl: "assets/7010826_3255307.png",
      backgroundColor: Colors.green.shade400),
  Domain(
      name: "Web-Development",
      imageUrl: "assets/7010826_3255307.png",
      backgroundColor: Colors.orange.shade400),
];
*/
List<Project> projects = [
  Project(
      name: "Project 1",
      imageUrl: "assets/7010826_3255307.png",
      backgroundColor: Colors.purple.shade400),
  Project(
      name: "Project 2",
      imageUrl: "assets/7010826_3255307.png",
      backgroundColor: Colors.yellow.shade400),
  Project(
      name: "Project 3",
      imageUrl: "assets/7010826_3255307.png",
      backgroundColor: Colors.redAccent.shade400),
  Project(
      name: "Project 4",
      imageUrl: "assets/7010826_3255307.png",
      backgroundColor: Colors.indigo.shade400),
];

List<String> title = ["Domains", "Projects Gallery"];
List<String> domains = [
  "AI/ML",
  "Flutter",
  "Programming Language",
  "Web-Development"
];
List<String> projectNames = [
  "Project 1",
  "Project 2",
  "Project 3",
  "Project 4"
];

class Educourse extends StatefulWidget {
  const Educourse({Key? key}) : super(key: key);

  @override
  State createState() => EducourseState();
}

class EducourseState extends State<Educourse> {
  void goToViewAll(List<Domain> itemList) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ViewAll(),
      ),
    );
  }

  Future<void> fillValues() async {
    aiml = await getDomainSpecificProjects(domains[0]);
    flutter = await getDomainSpecificProjects(domains[1]);
    prog = await getDomainSpecificProjects(domains[2]);
    web = await getDomainSpecificProjects(domains[3]);
  }

  @override
  Widget build(BuildContext context) {
    fillValues();
    return Scaffold(
      backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 47),
          const Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Row(
              children: [
                Icon(
                  Icons.menu,
                  size: 26.0,
                ),
                Spacer(),
                Icon(
                  Icons.notifications_none,
                  size: 26.0,
                ),
              ],
            ),
          ),
          const SizedBox(height: 19),
          const Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Text(
              "Welcome to",
              style: TextStyle(
                fontSize: 26.98,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Text(
              "Uni-CONNECT",
              style: TextStyle(
                fontSize: 37,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          const SizedBox(height: 15),
          const SizedBox(height: 29),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(left: 10, right: 10),
              decoration: const BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 1),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(38),
                  topRight: Radius.circular(38),
                ),
              ),
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: ListView(scrollDirection: Axis.vertical, children: [
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 15),
                        Row(
                          children: [
                            Text(
                              title[0],
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            const Spacer(),
                            Container(
                              decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(20)),
                              child: GestureDetector(
                                onTap: () {
                                  goToViewAll(domains.cast<Domain>());
                                },
                                child: const Text(
                                  " View-All ",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                        SizedBox(
                            height: 242,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 20),
                                  child: GestureDetector(
                                    onTap: () {
                                      domainSpecificList = aiml;
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => DomainList(
                                            domainName: domains[0],
                                          ),
                                        ),
                                      );
                                    },
                                    child: Container(
                                      height: 220,
                                      width: 160,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(14),
                                        gradient: const LinearGradient(
                                          colors: [
                                            Color.fromRGBO(197, 4, 98, 1),
                                            Color.fromRGBO(80, 3, 112, 1),
                                          ],
                                        ),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 20,
                                              left: 22,
                                              right: 18,
                                            ),
                                            child: Text(
                                              domains[0],
                                              style: const TextStyle(
                                                color: Color.fromRGBO(
                                                  255,
                                                  255,
                                                  255,
                                                  1,
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(height: 3),
                                          Expanded(
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Image.asset(
                                                "assets/7010826_3255307.png",
                                                height: 160,
                                                width: 180,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 20),
                                  child: GestureDetector(
                                    onTap: () {
                                      domainSpecificList = flutter;
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => DomainList(
                                            domainName: domains[1],
                                          ),
                                        ),
                                      );
                                    },
                                    child: Container(
                                      height: 220,
                                      width: 160,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(14),
                                        gradient: const LinearGradient(
                                          colors: [
                                            Color.fromRGBO(197, 4, 98, 1),
                                            Color.fromRGBO(80, 3, 112, 1),
                                          ],
                                        ),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 20,
                                              left: 22,
                                              right: 18,
                                            ),
                                            child: Text(
                                              domains[1],
                                              style: const TextStyle(
                                                color: Color.fromRGBO(
                                                  255,
                                                  255,
                                                  255,
                                                  1,
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(height: 3),
                                          Expanded(
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Image.asset(
                                                "assets/7010826_3255307.png",
                                                height: 160,
                                                width: 180,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )),
                      ]),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 242,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: GestureDetector(
                            onTap: () {
                              domainSpecificList = prog;
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => DomainList(
                                    domainName: domains[2],
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              height: 220,
                              width: 160,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                gradient: const LinearGradient(
                                  colors: [
                                    Color.fromRGBO(197, 4, 98, 1),
                                    Color.fromRGBO(80, 3, 112, 1),
                                  ],
                                ),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 22,
                                      right: 18,
                                    ),
                                    child: Text(
                                      domains[2],
                                      style: const TextStyle(
                                        color: Color.fromRGBO(
                                          255,
                                          255,
                                          255,
                                          1,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 3),
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: Image.asset(
                                        "assets/7010826_3255307.png",
                                        height: 160,
                                        width: 180,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: GestureDetector(
                            onTap: () {
                              domainSpecificList = web;
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => DomainList(
                                    domainName: domains[3],
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              height: 220,
                              width: 160,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                gradient: const LinearGradient(
                                  colors: [
                                    Color.fromRGBO(197, 4, 98, 1),
                                    Color.fromRGBO(80, 3, 112, 1),
                                  ],
                                ),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 22,
                                      right: 18,
                                    ),
                                    child: Text(
                                      domains[3],
                                      style: const TextStyle(
                                        color: Color.fromRGBO(
                                          255,
                                          255,
                                          255,
                                          1,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 3),
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: Image.asset(
                                        "assets/7010826_3255307.png",
                                        height: 160,
                                        width: 180,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      const SizedBox(
                        height: 25,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              width: 1.0,
                            ),
                            borderRadius: BorderRadius.circular(20)),
                        child: const Text(
                          "  Contact US!!  ",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              width: 1.0,
                            ),
                            borderRadius: BorderRadius.circular(20)),
                        child: GestureDetector(
                          onTap: () {},
                          child: const Text(
                            "  Visit Git-Hub  ",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

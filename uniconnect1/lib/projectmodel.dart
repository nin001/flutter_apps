import 'dart:typed_data';

class UniModal {
  int? id;
  final String projectTitle;
  final String description;
  final String college;
  final String gitLink;
  final String domain;
  final Uint8List pic1;
  final Uint8List pic2;
  final Uint8List pic3;
  final Uint8List pic4;

  UniModal({
    this.id,
    required this.projectTitle,
    required this.description,
    required this.college,
    required this.gitLink,
    required this.domain,
    required this.pic1,
    required this.pic2,
    required this.pic3,
    required this.pic4,
  });

  Map<String, dynamic> getMap() {
    return <String, dynamic>{
      'id': id,
      'projectTitle': projectTitle,
      'description': description,
      'domain': domain,
      'college': college,
      'gitLink': gitLink,
      'pic1': pic1,
      'pic2': pic2,
      'pic3': pic3,
      'pic4': pic4,
    };
  }
}

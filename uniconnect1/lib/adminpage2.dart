import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:image_picker_platform_interface/image_picker_platform_interface.dart';
import 'package:uniconnect1/getstarted.dart';
import 'package:uniconnect1/projectmodel.dart';
import 'package:google_fonts/google_fonts.dart';
/*
class Adminpage extends StatefulWidget {
  const Adminpage({super.key});

  @override
  State createState() => _AdminPageState();
}

class _AdminPageState extends State {
  TextEditingController titleController = TextEditingController();
  TextEditingController dataController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  List<File?> _selectedImages = [null, null, null];

  Future<void> _pickImage(int index) async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _selectedImages[index] = File(pickedFile.path);
      }
    });
  }

  Future<void> modalBottomSheet(bool doedit, [ToDoModel? toDoModelobj]) async {
    await showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(30))),
        context: this.context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
              left: 15,
              right: 15,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              const SizedBox(
                height: 13,
              ),
              const Text(
                "Add Projects Here!!",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 22,
                ),
              ),
              const SizedBox(height: 8),
              Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const DropdownMenu(
                        width: double.infinity,
                        hintText: 'Select The Domain',
                        dropdownMenuEntries: [
                          DropdownMenuEntry(value: (), label: 'AI/ML'),
                          DropdownMenuEntry(value: (), label: 'Flutter'),
                          DropdownMenuEntry(
                              value: (), label: 'Programming Language'),
                          DropdownMenuEntry(value: (), label: 'Tech Trends')
                        ]),
                    const Text(
                      "Title",
                      style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(89, 57, 241, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    TextFormField(
                      controller: titleController,
                      decoration: InputDecoration(
                        hintText: "Enter Project Title",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromARGB(255, 244, 67, 54)),
                        ),
                      ),
                    ),
                    const SizedBox(height: 8),
                    const Text(
                      "Description",
                      style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(89, 57, 241, 1),
                      ),
                    ),
                    const SizedBox(height: 3),
                    TextFormField(
                      controller: dataController,
                      decoration: InputDecoration(
                        hintText: "Enter Description",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromARGB(255, 244, 67, 54)),
                        ),
                      ),
                    ),
                    const SizedBox(height: 8),
                    const Text(
                      "Select Output Image 1",
                      style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(89, 57, 241, 1),
                      ),
                    ),
                    const SizedBox(height: 3),
                    for (int i = 0; i < 3; i++)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Select Output Image ${i + 1}",
                            style: const TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(89, 57, 241, 1),
                            ),
                          ),
                          const SizedBox(height: 3),
                          ListTile(
                            title: _selectedImages[i] == null
                                ? const Text('Pick Image')
                                : Image.file(_selectedImages[i]!),
                            onTap: () => _pickImage(i),
                          ),
                          const SizedBox(height: 8),
                        ],
                      ),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              Container(
                height: 50,
                width: 300,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(30)),
                child: ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      backgroundColor: const Color.fromRGBO(89, 57, 241, 1)),
                  child: const Text(
                    "Submit",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 22,
                      color: Color.fromRGBO(255, 255, 255, 1),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20),
            ]),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    List projectlist = [];
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(197, 4, 98, 1),
                Color.fromRGBO(80, 3, 112, 1),
              ],
            ),
          ),
          child: Scaffold(
            body: Padding(
              padding: const EdgeInsets.only(top: 45),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Row(
                    children: [
                      SizedBox(
                        width: 29,
                      ),
                      Text(
                        "Welcome",
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(
                        width: 29,
                      ),
                      Text(
                        "Admin",
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 41,
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40),
                        ),
                        color: Color.fromRGBO(217, 217, 217, 1),
                      ),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 19,
                          ),
                          const Text(
                            "Add the Project Details ",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          const SizedBox(
                            height: 18,
                          ),
                          Expanded(
                            child: Container(
                              padding: const EdgeInsets.only(top: 20),
                              decoration: const BoxDecoration(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  topRight: Radius.circular(40),
                                ),
                              ),
                              child: RefreshIndicator(
                                onRefresh: () async {
                                  taskList = await getValues();
                                  setState(() {});
                                },
                                child: ListView.builder(
                                    scrollDirection: Axis.vertical,
                                    itemCount: projectlist.length,
                                    itemBuilder: (context, index) {
                                      return Slidable(
                                        closeOnScroll: true,
                                        endActionPane: ActionPane(
                                          extentRatio: 0.2,
                                          motion: const DrawerMotion(),
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  GestureDetector(
                                                    onTap: () {},
                                                    child: Container(
                                                      height: 40,
                                                      width: 40,
                                                      decoration: BoxDecoration(
                                                          gradient:
                                                              const LinearGradient(
                                                            colors: [
                                                              Color.fromRGBO(
                                                                  197,
                                                                  4,
                                                                  98,
                                                                  1),
                                                              Color.fromRGBO(80,
                                                                  3, 112, 1),
                                                            ],
                                                          ),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      20)),
                                                      child: const Icon(
                                                        Icons.edit,
                                                        color: Colors.white,
                                                        size: 20,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {},
                                              child: Container(
                                                height: 40,
                                                width: 40,
                                                decoration: BoxDecoration(
                                                    gradient:
                                                        const LinearGradient(
                                                      colors: [
                                                        Color.fromRGBO(
                                                            197, 4, 98, 1),
                                                        Color.fromRGBO(
                                                            80, 3, 112, 1),
                                                      ],
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                child: const Icon(
                                                  Icons.delete,
                                                  color: Colors.white,
                                                  size: 20,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        key: ValueKey(index),
                                        child: Container(
                                          margin:
                                              const EdgeInsets.only(top: 10),
                                          padding: const EdgeInsets.only(
                                              top: 12, left: 20),
                                          decoration: BoxDecoration(
                                            color: const Color.fromRGBO(
                                                255, 255, 255, 1),
                                            border: Border.all(
                                              width: 0.5,
                                              color: const Color.fromRGBO(
                                                  0, 0, 0, 0.05),
                                            ),
                                            boxShadow: const [
                                              BoxShadow(
                                                  offset: Offset(0, 4),
                                                  blurRadius: 20,
                                                  color: Color.fromRGBO(
                                                      0, 0, 0, 0.13))
                                            ],
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.zero),
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                      bottom: 10,
                                                    ),
                                                    child: Container(
                                                      height: 60,
                                                      width: 60,
                                                      decoration:
                                                          const BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: Color.fromRGBO(
                                                            217, 217, 217, 1),
                                                      ),
                                                      child: Image.asset(
                                                          'assets/taskimage.jpeg'),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 20,
                                                  ),
                                                  SizedBox(
                                                    width: 240,
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          taskList[index].title,
                                                          style:
                                                              const TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w500,
                                                                  fontSize: 15,
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          0,
                                                                          0,
                                                                          0,
                                                                          1)),
                                                        ),
                                                        const SizedBox(
                                                          height: 5,
                                                        ),
                                                        Text(
                                                          taskList[index]
                                                              .description,
                                                          style:
                                                              const TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 12,
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          0,
                                                                          0,
                                                                          0,
                                                                          1)),
                                                        ),
                                                        const SizedBox(
                                                          height: 5,
                                                        ),
                                                        Text(
                                                          taskList[index].date,
                                                          style:
                                                              const TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 12,
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          0,
                                                                          0,
                                                                          0,
                                                                          1)),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  Checkbox(
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  10)),
                                                      activeColor:
                                                          const Color.fromRGBO(
                                                              4, 189, 0, 1),
                                                      value: taskList[index]
                                                          .isTaskDone,
                                                      onChanged: (val) {
                                                        setState(() {});
                                                      }),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    }),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
              shape: const CircleBorder(side: BorderSide.none),
              onPressed: () async {
                //clearController();
                await modalBottomSheet(false);
              },
              child: const Icon(
                Icons.add,
                size: 50,
                color: Color.fromRGBO(255, 255, 255, 1),
              ),
            ),
          ),
        ));
  }
}

*/

/*
//First update code for displaying properly


//In this code the bottom sheet is working properly but the 3 image pickers are missing here

class Adminpage extends StatefulWidget {
  const Adminpage({Key? key}) : super(key: key);

  @override
  State createState() => _AdminPageState();
}

class _AdminPageState extends State<Adminpage> {
  TextEditingController titleController = TextEditingController();
  TextEditingController dataController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  List<File?> _selectedImages = [null, null, null];

  Future<void> _pickImage(int index) async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _selectedImages[index] = File(pickedFile.path);
      }
    });
  }

  Future<void> modalBottomSheet(bool doedit, [ToDoModel? toDoModelobj]) async {
    await showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(30),
        ),
      ),
      context: context,
      builder: (context) {
        return SingleChildScrollView(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 13),
                const Text(
                  "Add Projects Here!!",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                  ),
                ),
                const SizedBox(height: 8),
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      DropdownButtonFormField(
                        items: const [
                          DropdownMenuItem(
                              value: 'AI/ML', child: Text('AI/ML')),
                          DropdownMenuItem(
                              value: 'Flutter', child: Text('Flutter')),
                          DropdownMenuItem(
                              value: 'Programming Language',
                              child: Text('Programming Language')),
                          DropdownMenuItem(
                              value: 'Tech Trends', child: Text('Tech Trends')),
                        ],
                        onChanged: (value) {},
                        decoration: InputDecoration(
                          hintText: 'Select The Domain',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      const Text(
                        "Title",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(89, 57, 241, 1),
                        ),
                      ),
                      const SizedBox(height: 3),
                      TextFormField(
                        controller: titleController,
                        decoration: InputDecoration(
                          hintText: "Enter Project Title",
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromARGB(255, 244, 67, 54),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      const Text(
                        "Description",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(89, 57, 241, 1),
                        ),
                      ),
                      const SizedBox(height: 3),
                      TextFormField(
                        controller: dataController,
                        decoration: InputDecoration(
                          hintText: "Enter Description",
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromARGB(255, 244, 67, 54),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      const Text(
                        "Select Output Image 1",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(89, 57, 241, 1),
                        ),
                      ),
                      const SizedBox(height: 3),
                      // Add image pickers here
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
                  ),
                  child: const Text(
                    "Submit",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 22,
                      color: Color.fromRGBO(255, 255, 255, 1),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color.fromRGBO(197, 4, 98, 1),
              Color.fromRGBO(80, 3, 112, 1),
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 45),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const Row(
                children: [
                  SizedBox(width: 29),
                  Text(
                    "Welcome",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(width: 29),
                  Text(
                    "Admin",
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 41),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40),
                    ),
                    color: Color.fromRGBO(217, 217, 217, 1),
                  ),
                  child: Column(
                    children: [
                      const SizedBox(height: 19),
                      const Text(
                        "Add the Project Details ",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(height: 18),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.only(top: 20),
                          decoration: const BoxDecoration(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                            ),
                          ),
                          // Remove ListView.builder from here
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
        shape: const CircleBorder(side: BorderSide.none),
        onPressed: () async {
          await modalBottomSheet(false);
        },
        child: const Icon(
          Icons.add,
          size: 50,
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
      ),
    );
  }
}
*/

//Third update of code

//Here all of the parameters are working fine!!!!!!!!!

const List<String> dropDownList = [
  "AI/ML",
  "Flutter",
  "Programming Language",
  "Web-Development"
];

class Adminpage extends StatefulWidget {
  const Adminpage({Key? key}) : super(key: key);

  @override
  State createState() => _AdminPageState();
}

class _AdminPageState extends State<Adminpage> {
  String dropDownValue = dropDownList.first;

  TextEditingController titleController = TextEditingController();
  TextEditingController dataController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  List<File?> _selectedImages = [null, null, null];

  Future<void> _pickImage(int index) async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _selectedImages[index] = File(pickedFile.path);
      }
    });
  }

  Future<void> modalBottomSheet(bool doedit, [UniModal? project]) async {
    await showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(30),
        ),
      ),
      context: context,
      builder: (context) {
        return SingleChildScrollView(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 13),
                const Text(
                  "Add Projects Here!!",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                  ),
                ),
                const SizedBox(height: 8),
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      DropdownButton(
                        elevation: 10,
                        value: dropDownValue,
                        onChanged: (value) {
                          dropDownValue = value.toString();
                          setState(() {});
                        },
                        onTap: () {
                          setState(() {});
                        },
                        items: dropDownList
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                              onTap: () {
                                setState(() {});
                              },
                              value: value,
                              child: Text(
                                value,
                                style: const TextStyle(
                                    color: Color.fromARGB(255, 1, 18, 21),
                                    fontSize: 17),
                              ));
                        }).toList(),
                      ),
                      const SizedBox(height: 8),
                      const Text(
                        "Title",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(89, 57, 241, 1),
                        ),
                      ),
                      const SizedBox(height: 3),
                      TextFormField(
                        controller: titleController,
                        decoration: InputDecoration(
                          hintText: "Enter Project Title",
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromARGB(255, 244, 67, 54),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      const Text(
                        "Description",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(89, 57, 241, 1),
                        ),
                      ),
                      const SizedBox(height: 3),
                      TextFormField(
                        controller: dataController,
                        decoration: InputDecoration(
                          hintText: "Enter Description",
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromARGB(255, 244, 67, 54),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      const Text(
                        "Select Output Image 1",
                        style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(89, 57, 241, 1),
                        ),
                      ),
                      const SizedBox(height: 3),
                      Row(
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              onPressed: () => _pickImage(0),
                              child: const Text('Pick Image 1'),
                            ),
                          ),
                          const SizedBox(width: 8),
                          Expanded(
                            child: ElevatedButton(
                              onPressed: () => _pickImage(1),
                              child: const Text('Pick Image 2'),
                            ),
                          ),
                          const SizedBox(width: 8),
                          Expanded(
                            child: ElevatedButton(
                              onPressed: () => _pickImage(2),
                              child: const Text('Pick Image 3'),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () async {
                    // Admin Page insertion
                    /* Structure
                    UniModal obj = UniModal(
                      projectTitle: projectTitle, 
                      description: description, 
                      college: college, 
                      gitLink: gitLink, 
                      domain: domain, 
                      pic1: pic1, 
                      pic2: pic2, 
                      pic3: pic3, 
                      pic4: pic4)*/
                    if (!doedit) {
                      String projectTitle = titleController.text;
                      String description = dataController.text;
                      String college = ""; // not present in the BottomSHeet
                      String gitLink = ""; // not present in the BottomSHeet
                      String domain = dropDownValue;
                      Uint8List pic1 = _selectedImages[0]!.readAsBytesSync();
                      Uint8List pic2 = _selectedImages[1]!.readAsBytesSync();
                      Uint8List pic3 = _selectedImages[2]!.readAsBytesSync();
                      Uint8List pic4 = _selectedImages[2]!.readAsBytesSync();

                      UniModal project = UniModal(
                          projectTitle: projectTitle,
                          description: description,
                          college: college,
                          gitLink: gitLink,
                          domain: domain,
                          pic1: pic1,
                          pic2: pic2,
                          pic3: pic3,
                          pic4: pic4);
                      insertProject(project);
                      log("${projectList.length}");
                      log(projectList.toString());
                      projectList = await getAllProjects();
                    } else {
                      String projectTitle = titleController.text;
                      String description = dataController.text;
                      String college = ""; // not present in the BottomSHeet
                      String gitLink = ""; // not present in the BottomSHeet
                      String domain = project!.domain;
                      Uint8List pic1 = _selectedImages[0]!.readAsBytesSync();
                      Uint8List pic2 = _selectedImages[1]!.readAsBytesSync();
                      Uint8List pic3 = _selectedImages[2]!.readAsBytesSync();
                      Uint8List pic4 = _selectedImages[2]!.readAsBytesSync();

                      UniModal projectEdit = UniModal(
                          id: project.id,
                          projectTitle: projectTitle,
                          description: description,
                          college: college,
                          gitLink: gitLink,
                          domain: domain,
                          pic1: pic1,
                          pic2: pic2,
                          pic3: pic3,
                          pic4: pic4);

                      updateProject(projectEdit);
                      log("${projectList.length}");
                      log(projectList.toString());
                      projectList = await getAllProjects();
                    }
                    setState(() {});
                    Navigator.of(context).pop();
                    titleController.clear();
                    dataController.clear();
                    dropDownValue = dropDownList.first;
                    _selectedImages = [null, null, null];
                  },
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
                  ),
                  child: const Text(
                    "Submit",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 22,
                      color: Color.fromRGBO(255, 255, 255, 1),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> editProject(UniModal toDoModelobj) async {
    dropDownValue = toDoModelobj.domain;
    titleController.text = toDoModelobj.projectTitle;
    dataController.text = toDoModelobj.description;
    await modalBottomSheet(true, toDoModelobj);
    setState(() {});
  }

  void removeProject(UniModal project) async {
    await deleteProject(project);
    projectList = await getAllProjects();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color.fromRGBO(197, 4, 98, 1),
              Color.fromRGBO(80, 3, 112, 1),
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 45),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const Row(
                children: [
                  SizedBox(width: 29),
                  Text(
                    "Welcome",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(width: 29),
                  Text(
                    "Admin",
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 41),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40),
                    ),
                    color: Color.fromRGBO(217, 217, 217, 1),
                  ),
                  child: Column(
                    children: [
                      const SizedBox(height: 19),
                      const Text(
                        "Add the Project Details ",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(height: 18),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.only(top: 20),
                          decoration: const BoxDecoration(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                            ),
                          ),
                          // Removed ListView.builder from here
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: 10, right: 10, top: 10, bottom: 20),
                            child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                itemCount: projectList.length,
                                itemBuilder: (context, index) {
                                  return Slidable(
                                    closeOnScroll: true,
                                    endActionPane: ActionPane(
                                        extentRatio: 0.2,
                                        motion: const DrawerMotion(),
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  GestureDetector(
                                                    onTap: () {
                                                      editProject(
                                                          projectList[index]);
                                                    },
                                                    child: Container(
                                                      height: 40,
                                                      width: 40,
                                                      decoration: BoxDecoration(
                                                          color: const Color
                                                              .fromRGBO(
                                                              89, 57, 241, 1),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      20)),
                                                      child: const Icon(
                                                        Icons.edit,
                                                        color: Colors.white,
                                                        size: 20,
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      removeProject(
                                                          projectList[index]);
                                                    },
                                                    child: Container(
                                                      height: 40,
                                                      width: 40,
                                                      decoration: BoxDecoration(
                                                          color: const Color
                                                              .fromRGBO(
                                                              89, 57, 241, 1),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      20)),
                                                      child: const Icon(
                                                        Icons.delete,
                                                        color: Colors.white,
                                                        size: 20,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ]),
                                    key: ValueKey(index),
                                    child: Container(
                                      margin: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      padding: const EdgeInsets.only(
                                        left: 20,
                                        top: 12,
                                      ),
                                      decoration: BoxDecoration(
                                        color: const Color.fromRGBO(
                                            255, 255, 255, 1),
                                        border: Border.all(
                                          width: 0.5,
                                          color: const Color.fromRGBO(
                                              0, 0, 0, 0.05),
                                        ),
                                        boxShadow: const [
                                          BoxShadow(
                                              offset: Offset(0, 4),
                                              blurRadius: 20,
                                              color:
                                                  Color.fromRGBO(0, 0, 0, 0.13))
                                        ],
                                        borderRadius:
                                            const BorderRadius.all(Radius.zero),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 10),
                                                child: Container(
                                                    height: 60,
                                                    width: 60,
                                                    decoration:
                                                        const BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: Color.fromRGBO(
                                                          217, 217, 217, 1),
                                                    ),
                                                    child: Image.memory(
                                                        projectList[index]
                                                            .pic1)),
                                              ),
                                              const SizedBox(
                                                width: 20,
                                              ),
                                              SizedBox(
                                                width: 200,
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      projectList[index]
                                                          .projectTitle,
                                                      style: GoogleFonts.inter(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 15,
                                                          color: const Color
                                                              .fromRGBO(
                                                              0, 0, 0, 1)),
                                                    ),
                                                    const SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      "${projectList[index].description.substring(0, 70)}....",
                                                      style: GoogleFonts.inter(
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 12,
                                                          color: const Color
                                                              .fromRGBO(
                                                              0, 0, 0, 1)),
                                                    ),
                                                    const SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      projectList[index].domain,
                                                      style: GoogleFonts.inter(
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 12,
                                                          color: const Color
                                                              .fromRGBO(
                                                              0, 0, 0, 1)),
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
        shape: const CircleBorder(side: BorderSide.none),
        onPressed: () async {
          await modalBottomSheet(false);
        },
        child: const Icon(
          Icons.add,
          size: 50,
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
      ),
    );
  }
}

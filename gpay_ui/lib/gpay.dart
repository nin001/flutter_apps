import 'package:flutter/material.dart';

class Gpay extends StatelessWidget {
  const Gpay({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pay merchent or friends "),
        actions: [
          const Icon(
            Icons.search,
            size: 45,
          ),
          Image.network(
              "https://cdn-icons-png.flaticon.com/128/6124/6124998.png"),
          const SizedBox(
            width: 2,
          )
        ],
        backgroundColor: Colors.white54,
        centerTitle: true,
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Image.asset(
            "images/first.jpg",
            height: 200,
            fit: BoxFit.fill,
          ),
          const SizedBox(
            height: 10,
          ),
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                      height: 100,
                      width: 100,
                      child: Column(
                        children: [
                          Image.asset(
                            "images/qr.png",
                            alignment: Alignment.topCenter,
                            height: 40,
                            width: 40,
                          ),
                          const Text(
                            "Scan any QR Code",
                            style: TextStyle(
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )),
                  SizedBox(
                      height: 100,
                      width: 100,
                      child: Column(
                        children: [
                          Image.asset(
                            "images/payContacts.png",
                            alignment: Alignment.topCenter,
                            height: 40,
                            width: 40,
                          ),
                          const Text(
                            "Pay Contacts",
                            style: TextStyle(
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )),
                  SizedBox(
                      height: 100,
                      width: 100,
                      child: Column(
                        children: [
                          Image.asset(
                            "images/payPhone.png",
                            alignment: Alignment.topCenter,
                            height: 40,
                            width: 40,
                          ),
                          const Text(
                            "Pay Phone Number",
                            style: TextStyle(
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                      height: 100,
                      width: 100,
                      child: Column(
                        children: [
                          Image.asset(
                            "images/bank.png",
                            alignment: Alignment.topCenter,
                            height: 40,
                            width: 40,
                          ),
                          const Text(
                            "Bank Transfer",
                            style: TextStyle(
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )),
                  SizedBox(
                      height: 100, //
                      width: 100,
                      child: Column(
                        children: [
                          Image.asset(
                            "images/payUPI.png",
                            alignment: Alignment.topCenter,
                            height: 40,
                            width: 40,
                          ),
                          const Text(
                            "Pay Upi Id or Number",
                            style: TextStyle(
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )),
                  SizedBox(
                      height: 100,
                      width: 100,
                      child: Column(
                        children: [
                          Image.asset(
                            "images/selfTransfer.png",
                            alignment: Alignment.topCenter,
                            height: 40,
                            width: 40,
                          ),
                          const Text(
                            "Self Transfer",
                            style: TextStyle(
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                      height: 100,
                      width: 100,
                      child: Column(
                        children: [
                          Image.asset(
                            "images/bill.png",
                            alignment: Alignment.topCenter,
                            height: 40,
                            width: 40,
                          ),
                          const Text(
                            "Pay Bills",
                            style: TextStyle(
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )),
                  SizedBox(
                      height: 100,
                      width: 100,
                      child: Column(
                        children: [
                          Image.asset(
                            "images/mobile.png",
                            alignment: Alignment.topCenter,
                            height: 40,
                            width: 40,
                          ),
                          const Text(
                            "Mobile Recharge",
                            style: TextStyle(
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )),
                  Container(
                    height: 100,
                    width: 100,
                    color: Colors.white,
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            "  People",
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w400),
            textAlign: TextAlign.left,
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/profile.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/profile.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/profile.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/profile.png",
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/profile.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/profile.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/profile.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.network(
                  "https://cdn-icons-png.flaticon.com/128/2722/2722987.png",
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            "  Business",
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w400),
            textAlign: TextAlign.left,
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/businessProfile.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/businessProfile.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/businessProfile.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.network(
                  "https://cdn-icons-png.flaticon.com/128/2722/2722987.png",
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            "  Bills and Recharges",
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w400),
            textAlign: TextAlign.left,
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/dth.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/electricity.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/recharge.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/router.png",
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            "  Offers And Rewards ",
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w400),
            textAlign: TextAlign.left,
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/reward.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/offer.png",
                ),
              ),
              SizedBox(
                height: 65,
                width: 65,
                child: Image.asset(
                  "images/reference.png",
                  alignment: Alignment.center,
                ),
              ),
              const SizedBox(
                height: 85,
                width: 85,
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Image.asset("images/bottom.jpg")
        ],
      ),
    );
  }
}

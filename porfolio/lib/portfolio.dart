import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Portfolio extends StatelessWidget {
  const Portfolio({super.key});

  @override
  Widget build(BuildContext context) {
    final Uri insta = Uri.parse(
        "https://www.instagram.com/niraj_1992_?igsh=MWVteW51dWpnZ3BoYg==");
    final Uri linkedin = Uri.parse(
        "https://www.linkedin.com/in/niraj-randhir-5b5b15215?utm_source=share&utm_campaign=share_via&utm_content=profile&utm_medium=android_app");
    final Uri gitlab = Uri.parse("https://gitlab.com/nin001");

    final Uri c2w = Uri.parse("https://core2web.in/");
    Future<void> launchInsta() async {
      if (!await launchUrl(insta)) {
        throw Exception('Could not launch $insta');
      }
    }

    Future<void> launchLinkedIn() async {
      if (!await launchUrl(linkedin)) {
        throw Exception('Could not launch $linkedin');
      }
    }

    Future<void> launchC2W() async {
      if (!await launchUrl(c2w)) {
        throw Exception('could not launch c2w');
      }
    }

    Future<void> launchGitLab() async {
      if (!await launchUrl(gitlab)) {
        throw Exception('Could not launch $gitlab');
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Portfolio",
          style: TextStyle(
            color: Color.fromARGB(230, 6, 23, 18),
            fontSize: 30,
            letterSpacing: 1,
          ),
        ),
        centerTitle: true,
        backgroundColor: Color.fromARGB(255, 12, 223, 209),
        actions: [
          IconButton(
            onPressed: launchLinkedIn,
            icon: Image.network(
              "https://cdn1.iconfinder.com/data/icons/logotypes/32/circle-linkedin-512.png",
              height: 45,
              width: 45,
              fit: BoxFit.contain,
            ),
          ),
          IconButton(
            onPressed: launchInsta,
            icon: Image.network(
              "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Instagram_icon.png/900px-Instagram_icon.png?20200512141346",
              height: 45,
              width: 45,
              fit: BoxFit.contain,
            ),
          ),
          IconButton(
              onPressed: launchGitLab,
              icon: Image.network(
                "https://cdn.iconscout.com/icon/premium/png-512-thumb/gitlab-2752181-2284998.png?f=webp&w=256",
                height: 45,
                width: 45,
                fit: BoxFit.contain,
              ))
        ],
      ),
      body: Container(
        color: Color.fromARGB(164, 64, 213, 196),
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            const SizedBox(
              height: 10,
            ),
            const Text(
              "Name : Niraj Randhir",
              style: TextStyle(
                  color: Color.fromARGB(230, 6, 23, 18),
                  fontSize: 25,
                  fontWeight: FontWeight.w500),
              textAlign: TextAlign.start,
            ),
            const SizedBox(
              height: 20,
            ),
            Image.asset(
              "images/nirajPicture.jpg",
              height: 350,
              width: 250,
              fit: BoxFit.contain,
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              "College : SCOE",
              style: TextStyle(
                  color: Color.fromARGB(230, 6, 23, 18),
                  fontSize: 25,
                  fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 20,
            ),
            Image.network(
              "https://www.stesracing.org/assets/img/Sinhgad%20Institutes%20custom.png",
              height: 120,
              width: 150,
              fit: BoxFit.contain,
              colorBlendMode: BlendMode.exclusion,
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              "Dream Company : Microsoft",
              style: TextStyle(
                  color: Color.fromARGB(230, 6, 23, 18),
                  fontSize: 25,
                  fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 10,
            ),
            Image.network(
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQY1z2E1R9N9c6bYhPYz7peyDvw0Nw1gpEcVY3rJY7MyQ&s",
              height: 120,
              width: 150,
              fit: BoxFit.contain,
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              "Projects Done",
              style: TextStyle(
                  color: Color.fromARGB(230, 6, 23, 18),
                  fontSize: 25,
                  fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 20,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  const SizedBox(
                    width: 20,
                  ),
                  Image.asset(
                    "images/project1.png",
                    height: 350,
                    width: 450,
                    fit: BoxFit.fill,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Image.asset(
                    "images/project2.jpg",
                    height: 350,
                    width: 250,
                    fit: BoxFit.fill,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Image.asset(
                    "images/project3.png",
                    height: 350,
                    width: 450,
                    fit: BoxFit.fill,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              "About me : ",
              style: TextStyle(
                  color: Color.fromARGB(230, 6, 23, 18),
                  fontSize: 25,
                  fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 20,
            ),
            Image.asset("images/description.png", height: 250, fit: BoxFit.fill)
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: launchC2W,
        child: const Icon(Icons.next_plan),
      ),
    );
  }
}

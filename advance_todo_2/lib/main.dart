import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';

String username = "Niraj";
String pass = "1234";

dynamic todoDatabase;

// List Global due to one time initialization
List<ToDoModel> taskList = [];
void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  todoDatabase = openDatabase(
    join(await getDatabasesPath(), "ToDoDatabase.db"),
    version: 1,
    onCreate: (db, version) {
      db.execute('''CREATE TABLE tasks(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT,
        description TEXT,
        date TEXT,
        isTaskDone INT
      )''');
      db.execute('''CREATE TABLE user_info(
        username TEXT PRIMARY KEY,
        password
      )''');
    },
  );
  taskList = await getValues();
  runApp(const MyApp());
}

void getUsername() async {
  Database localDB = await todoDatabase;

  List<Map<String, dynamic>> ret =
      await localDB.query('user_info', columns: ['username']);

  username = await ret[0]['username'];
}

void getPassword() async {
  Database localDB = await todoDatabase;

  List<Map<String, dynamic>> ret =
      await localDB.query('user_info', columns: ['password']);

  pass = await ret[0]['password'];
}

Future<void> insertIntoDatabase(ToDoModel obj) async {
  final localDB = await todoDatabase;

  await localDB.insert('tasks',
      obj.toMap(), //return <String,dynamic>{'title':title , 'description':description ...}
      conflictAlgorithm: ConflictAlgorithm.replace);

  taskList = await getValues();
}

Future<void> deleteFromDatabase(ToDoModel obj) async {
  final localDB = await todoDatabase;

  await localDB.delete('tasks', where: 'id = ?', whereArgs: [obj.id]);
  taskList = await getValues();
}

Future<void> updateIntoDatabase(ToDoModel obj) async {
  final localDB = await todoDatabase;

  await localDB
      .update('tasks', obj.toMap(), where: 'id = ?', whereArgs: [obj.id]);

  taskList = await getValues();
}

Future<List<ToDoModel>> getValues() async {
  final localDB = await todoDatabase;

  List<Map<String, dynamic>> list = await localDB.query('tasks');

  return List.generate(list.length, (index) {
    return ToDoModel(
        id: list[index]['id'],
        title: list[index]['title'],
        description: list[index]['description'],
        date: list[index]['date'],
        isTaskDone: (list[index]['isTaskDone'] == 0) ? false : true);
  });
}

//  db.execute('''CREATE TABLE tasks(
//         id INTEGER PRIMARY KEY AUTOINCREMENT,
//         title TEXT,
//         description TEXT,
//         date TEXT,
//         isTaskDone INT
//       )''');

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class ToDoListAdv extends StatefulWidget {
  const ToDoListAdv({super.key});

  @override
  State createState() => _ToDoListAdvState();
}

class _ToDoListAdvState extends State {
  TextEditingController titleController = TextEditingController();
  TextEditingController dataController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  Future<void> modalBottomSheet(bool doedit, [ToDoModel? toDoModelobj]) async {
    await showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(30))),
        context: this.context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
              left: 15,
              right: 15,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              const SizedBox(
                height: 13,
              ),
              Text(
                "Create Task",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.w600,
                  fontSize: 22,
                ),
              ),
              const SizedBox(height: 8),
              Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Title",
                      style: GoogleFonts.quicksand(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(89, 57, 241, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    TextFormField(
                      controller: titleController,
                      decoration: InputDecoration(
                        hintText: "Enter Title",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromARGB(255, 244, 67, 54)),
                        ),
                      ),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      "Description",
                      style: GoogleFonts.quicksand(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(89, 57, 241, 1),
                      ),
                    ),
                    const SizedBox(height: 3),
                    TextFormField(
                      controller: dataController,
                      decoration: InputDecoration(
                        hintText: "Enter Description",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromARGB(255, 244, 67, 54)),
                        ),
                      ),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      "Date",
                      style: GoogleFonts.quicksand(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(89, 57, 241, 1),
                      ),
                    ),
                    const SizedBox(height: 3),
                    TextField(
                      controller: dateController,
                      readOnly: true,
                      decoration: InputDecoration(
                        hintText: "Enter Date",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                              color: Color.fromARGB(255, 244, 67, 54)),
                        ),
                        suffixIcon: const Icon(Icons.date_range_rounded),
                      ),
                      onTap: () async {
                        DateTime? pickeddate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1924),
                          lastDate: DateTime(2025),
                        );
                        String formatedDate =
                            DateFormat.yMMMd().format(pickeddate!);
                        setState(() {
                          dateController.text = formatedDate;
                        });
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              Container(
                height: 50,
                width: 300,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(30)),
                child: ElevatedButton(
                  onPressed: () {
                    doedit ? submit(doedit, toDoModelobj) : submit(doedit);
                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      backgroundColor: const Color.fromRGBO(89, 57, 241, 1)),
                  child: Text(
                    "Submit",
                    style: GoogleFonts.quicksand(
                      fontWeight: FontWeight.w700,
                      fontSize: 22,
                      color: const Color.fromRGBO(255, 255, 255, 1),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20),
            ]),
          );
        });
  }

  void submit(bool doedit, [ToDoModel? toDoModelobj]) async {
    if (titleController.text.trim().isNotEmpty &&
        dataController.text.trim().isNotEmpty &&
        dateController.text.trim().isNotEmpty) {
      if (!doedit) {
        await insertIntoDatabase(ToDoModel(
          title: titleController.text.trim(),
          description: dataController.text.trim(),
          date: dateController.text.trim(),
          isTaskDone: false,
        ));

        setState(() {});
      } else {
        toDoModelobj!.date = dateController.text.trim();
        toDoModelobj.title = titleController.text.trim();
        toDoModelobj.description = dataController.text.trim();
        await updateIntoDatabase(toDoModelobj);
      }
    }
    clearController();
  }

  void clearController() {
    titleController.clear();
    dataController.clear();
    dateController.clear();
  }

  void removeTasks(ToDoModel toDoModelobj) async {
    await deleteFromDatabase(toDoModelobj);
    setState(() {});
  }

  Future<void> editTasks(ToDoModel toDoModelobj) async {
    titleController.text = toDoModelobj.title;
    dataController.text = toDoModelobj.description;
    dateController.text = toDoModelobj.date;
    await modalBottomSheet(true, toDoModelobj);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(111, 81, 255, 1),
      body: Padding(
        padding: const EdgeInsets.only(top: 45),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                const SizedBox(width: 29),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Welcome",
                      style: GoogleFonts.quicksand(
                        fontSize: 22,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(255, 255, 255, 1),
                      ),
                    ),
                    Text(
                      username,
                      style: GoogleFonts.quicksand(
                        fontSize: 30,
                        fontWeight: FontWeight.w600,
                        color: const Color.fromRGBO(255, 255, 255, 1),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 41),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                    color: Color.fromRGBO(217, 217, 217, 1),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40),
                    )),
                child: Column(children: [
                  const SizedBox(
                    height: 19,
                  ),
                  Text("Create To Do List",
                      style: GoogleFonts.quicksand(
                          fontSize: 15, fontWeight: FontWeight.w500)),
                  const SizedBox(
                    height: 18,
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.only(top: 20),
                      decoration: const BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40),
                        ),
                      ),
                      child: RefreshIndicator(
                        onRefresh: () async {
                          taskList = await getValues();
                          setState(() {});
                        },
                        child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            itemCount: taskList.length,
                            itemBuilder: (context, index) {
                              return Slidable(
                                closeOnScroll: true,
                                endActionPane: ActionPane(
                                    extentRatio: 0.2,
                                    motion: const DrawerMotion(),
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 10),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  editTasks(taskList[index]);
                                                },
                                                child: Container(
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                      color:
                                                          const Color.fromRGBO(
                                                              89, 57, 241, 1),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20)),
                                                  child: const Icon(
                                                    Icons.edit,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  removeTasks(taskList[index]);
                                                },
                                                child: Container(
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                      color:
                                                          const Color.fromRGBO(
                                                              89, 57, 241, 1),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20)),
                                                  child: const Icon(
                                                    Icons.delete,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ]),
                                key: ValueKey(index),
                                child: Container(
                                  margin: const EdgeInsets.only(
                                    top: 10,
                                  ),
                                  padding: const EdgeInsets.only(
                                    left: 20,
                                    top: 12,
                                  ),
                                  decoration: BoxDecoration(
                                    color:
                                        const Color.fromRGBO(255, 255, 255, 1),
                                    border: Border.all(
                                      width: 0.5,
                                      color:
                                          const Color.fromRGBO(0, 0, 0, 0.05),
                                    ),
                                    boxShadow: const [
                                      BoxShadow(
                                          offset: Offset(0, 4),
                                          blurRadius: 20,
                                          color: Color.fromRGBO(0, 0, 0, 0.13))
                                    ],
                                    borderRadius:
                                        const BorderRadius.all(Radius.zero),
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 10),
                                            child: Container(
                                              height: 60,
                                              width: 60,
                                              decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Color.fromRGBO(
                                                    217, 217, 217, 1),
                                              ),
                                              child: Image.asset(
                                                'assets/Group 42.png',
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 20,
                                          ),
                                          SizedBox(
                                            width: 200,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  taskList[index].title,
                                                  style: GoogleFonts.inter(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 15,
                                                      color:
                                                          const Color.fromRGBO(
                                                              0, 0, 0, 1)),
                                                ),
                                                const SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  taskList[index].description,
                                                  style: GoogleFonts.inter(
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 12,
                                                      color:
                                                          const Color.fromRGBO(
                                                              0, 0, 0, 1)),
                                                ),
                                                const SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  taskList[index].date,
                                                  style: GoogleFonts.inter(
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 12,
                                                      color:
                                                          const Color.fromRGBO(
                                                              0, 0, 0, 1)),
                                                ),
                                                const SizedBox(
                                                  height: 10,
                                                )
                                              ],
                                            ),
                                          ),
                                          Checkbox(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              activeColor: const Color.fromRGBO(
                                                  4, 189, 0, 1),
                                              value: taskList[index].isTaskDone,
                                              onChanged: (val) {
                                                setState(() {
                                                  if (taskList[index]
                                                      .isTaskDone) {
                                                    taskList[index].isTaskDone =
                                                        false; //0
                                                    setState(() {
                                                      updateIntoDatabase(
                                                          taskList[index]);
                                                    });
                                                  } else {
                                                    taskList[index].isTaskDone =
                                                        true; //1
                                                    setState(() {
                                                      updateIntoDatabase(
                                                          taskList[index]);
                                                    });
                                                  }
                                                });
                                              }),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                      ),
                    ),
                  ),
                ]),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
        shape: const CircleBorder(side: BorderSide.none),
        onPressed: () async {
          clearController();
          await modalBottomSheet(false);
        },
        child: const Icon(
          Icons.add,
          size: 50,
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
      ),
    );
  }
}

class ToDoModel {
  int? id;
  String title;
  String description;
  String date;

  bool isTaskDone;

  ToDoModel({
    this.id,
    required this.title,
    required this.description,
    required this.date,
    required this.isTaskDone,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'title': title,
      'description': description,
      'date': date,
      'isTaskDone': (isTaskDone) ? 1 : 0
    };

    //**if(isTaskDone) {
    //  value = 1
    //}else {
    //  value = 0;
    //} */
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  bool registered = false;

  @override
  void initState() {
    super.initState();
    checkIfRegistered();
  }

  Future<void> checkIfRegistered() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    bool r = preferences.getBool('registered') ?? false;

    setState(() {
      registered = r;
    });
  }

  static Future<void> onRegistrationComplete() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool('registered', true);
  }

  @override
  Widget build(BuildContext context) {
    if (registered) {
      return const LoginToDo();
    } else {
      return const RegistrationToDo();
    }
  }
}

class LoginToDo extends StatefulWidget {
  const LoginToDo({super.key});

  @override
  State createState() => _LoginState();
}

class _LoginState extends State {
  @override
  void initState() {
    super.initState();
    getUsername();
    getUsername();
  }

  // Global key for form
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController user = TextEditingController();
  final TextEditingController password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(32, 220, 230, 1),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 30, top: 50),
          child: Container(
            decoration: const BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(20))),
            padding: const EdgeInsets.only(left: 30, top: 50),
            child: Container(
                height: 1200,
                width: 500,
                decoration: const BoxDecoration(
                    color: Colors.greenAccent,
                    borderRadius:
                        BorderRadius.only(topLeft: Radius.circular(20))),
                child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.asset(
                                  'assets/profile.avif',
                                  height: 100,
                                  width: 90,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Login',
                                style: GoogleFonts.quicksand(
                                  fontSize: 25,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Username",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.quicksand(
                                fontSize: 20, fontWeight: FontWeight.w300),
                          ),
                          SizedBox(
                            height: 60,
                            // decoration: BoxDecoration(
                            //     color: Colors.white,
                            //     borderRadius: BorderRadius.circular()),
                            child: TextFormField(
                              controller: user,
                              decoration: InputDecoration(
                                  suffixIcon: const Icon(Icons.account_circle),
                                  border: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                  hintText: "Enter username",
                                  hintStyle: GoogleFonts.quicksand(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w400),
                                  fillColor: Colors.white,
                                  filled: true),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "Please Enter Username";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Password",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.quicksand(
                                fontSize: 20, fontWeight: FontWeight.w300),
                          ),
                          SizedBox(
                            height: 60,
                            // decoration: BoxDecoration(
                            //     color: Colors.white,
                            //     borderRadius: BorderRadius.circular()),
                            child: TextFormField(
                              controller: password,
                              obscureText: true,
                              obscuringCharacter: "*",
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  suffixIcon: const Icon(Icons.password),
                                  border: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                  hintText: "Enter Password",
                                  hintStyle: GoogleFonts.quicksand(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w400),
                                  fillColor: Colors.white,
                                  filled: true),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "Please Enter Username";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    bool validation =
                                        _formKey.currentState!.validate();
                                    print(username);
                                    print(pass);
                                    if (validation) {
                                      if (user.text == username &&
                                          password.text == pass) {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(const SnackBar(
                                          content: Text("Login Succesfull!"),
                                        ));
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  const ToDoListAdv(),
                                            ));
                                      } else if (user.text != username ||
                                          password.text != pass) {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(const SnackBar(
                                          content: Text(
                                              "Wrong Username or password..."),
                                        ));
                                        user.clear();
                                        password.clear();
                                      }
                                    } else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(const SnackBar(
                                        content: Text("Login Failed!"),
                                      ));
                                    }
                                  },
                                  child: Text(
                                    "Login",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600),
                                  )),
                            ],
                          )
                        ],
                      ),
                    ))),
          ),
        ),
      ),
    );
  }
}

class RegistrationToDo extends StatefulWidget {
  const RegistrationToDo({super.key});

  @override
  State createState() => _RegisterState();
}

class _RegisterState extends State {
  // Global key for form
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController user = TextEditingController();
  final TextEditingController password = TextEditingController();

  Future<void> insertUsername(UserModel user) async {
    final localDB = await todoDatabase;

    await localDB.insert(
      'user_info',
      user.toMap(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(32, 220, 230, 1),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 30, top: 50),
          child: Container(
            decoration: const BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(20))),
            padding: const EdgeInsets.only(left: 30, top: 50),
            child: Container(
                height: 1200,
                width: 500,
                decoration: const BoxDecoration(
                    color: Colors.greenAccent,
                    borderRadius:
                        BorderRadius.only(topLeft: Radius.circular(20))),
                child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.asset(
                                  'assets/profile.avif',
                                  height: 100,
                                  width: 90,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Welcome To To-Do App',
                                style: GoogleFonts.quicksand(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Register Yourself To To-Do App',
                                style: GoogleFonts.quicksand(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Username",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.quicksand(
                                fontSize: 20, fontWeight: FontWeight.w300),
                          ),
                          SizedBox(
                            height: 60,
                            // decoration: BoxDecoration(
                            //     color: Colors.white,
                            //     borderRadius: BorderRadius.circular()),
                            child: TextFormField(
                              controller: user,
                              decoration: InputDecoration(
                                  suffixIcon: const Icon(Icons.account_circle),
                                  border: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                  hintText: "Enter username",
                                  hintStyle: GoogleFonts.quicksand(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w400),
                                  fillColor: Colors.white,
                                  filled: true),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "Please Enter Username";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Password",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.quicksand(
                                fontSize: 20, fontWeight: FontWeight.w300),
                          ),
                          SizedBox(
                            height: 60,
                            // decoration: BoxDecoration(
                            //     color: Colors.white,
                            //     borderRadius: BorderRadius.circular()),
                            child: TextFormField(
                              controller: password,
                              obscureText: true,
                              obscuringCharacter: "*",
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  suffixIcon: const Icon(Icons.password),
                                  border: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                  hintText: "Enter Password",
                                  hintStyle: GoogleFonts.quicksand(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w400),
                                  fillColor: Colors.white,
                                  filled: true),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "Please Enter Password";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    bool validation =
                                        _formKey.currentState!.validate();
                                    if (validation) {
                                      insertUsername(UserModel(
                                          username: user.text,
                                          password: password.text));
                                      _HomePage.onRegistrationComplete();
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(const SnackBar(
                                        content:
                                            Text("Registration Succesfull!"),
                                      ));
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                const LoginToDo(),
                                          ));
                                    } else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(const SnackBar(
                                        content: Text("Registration Failed!"),
                                      ));
                                    }
                                  },
                                  child: Text(
                                    "Register",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600),
                                  )),
                            ],
                          )
                        ],
                      ),
                    ))),
          ),
        ),
      ),
    );
  }
}

class UserModel {
  final String username;
  final String password;

  const UserModel({required this.username, required this.password});

  Map<String, dynamic> toMap() {
    return {'username': username, 'password': password};
  }
}

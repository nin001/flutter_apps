import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const IndianFlag();
  }
}

class IndianFlag extends StatefulWidget {
  const IndianFlag({super.key});

  State<IndianFlag> createState() => _IndianFlag();
}

class _IndianFlag extends State<IndianFlag> {
  int _counter = 0;

  void increment() {
    setState(() {
      _counter++;
    });
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Happy Republic Day"),
          actions: [Icon(Icons.favorite_border_rounded)],
        ),
        body: Container(
            color: Colors.grey,
            height: double.infinity,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  children: [
                    const SizedBox(
                      height: 100,
                    ),
                    Method.showPole(_counter),
                  ],
                ),
                Column(
                  children: [
                    const SizedBox(
                      height: 100,
                    ),
                    Method.showOrange(_counter),
                    Method.showWhite(_counter),
                    Method.showGreen(_counter),
                  ],
                )
              ],
            )),
        floatingActionButton: FloatingActionButton(
          onPressed: increment,
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}

class Method {
  static Container showPole(int counter) {
    return (counter >= 1)
        ? Container(
            height: 500,
            width: 20,
            color: Colors.brown,
          )
        : Container();
  }

  static Container showOrange(int counter) {
    return (counter >= 2)
        ? Container(
            width: 300,
            height: 50,
            color: Colors.orange,
          )
        : Container();
  }

  static Container showWhite(int counter) {
    return (counter >= 3)
        ? Container(
            width: 300,
            height: 50,
            color: Colors.white,
            child: showChakra(counter),
          )
        : Container();
  }

  static Widget showChakra(int counter) {
    return (counter >= 4)
        ? Image.network(
            'https://t3.ftcdn.net/jpg/03/11/13/46/240_F_311134651_RXMvbUB3h089Js0ODvuHrttmsON9Tpik.jpg',
            height: 30,
            width: 30,
            fit: BoxFit.contain,
          )
        : Container();
  }

  static Container showGreen(int counter) {
    return (counter >= 5)
        ? Container(
            width: 300,
            height: 50,
            color: Colors.green,
          )
        : Container();
  }
}

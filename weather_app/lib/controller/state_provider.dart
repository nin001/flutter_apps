import 'package:flutter/material.dart';

class CityProvider extends ChangeNotifier {
  String city;

  CityProvider({required this.city});

  void updateValues(String city) {
    this.city = city;

    notifyListeners();
  }
}

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:weather/weather.dart';
import 'package:weather_app/controller/state_provider.dart';
import 'package:weather_app/main.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:weather_app/view/searchscreen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Weather? weather;
  late String city;

  @override
  void initState() {
    super.initState();
  }

  void setWeatherByCity() async {
    city = Provider.of<CityProvider>(context).city;
    await weatherFactory.currentWeatherByCityName(city).then((w) {
      setState(() {
        weather = w;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    setWeatherByCity();
    return Scaffold(
      body: _buildUI(),
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.white24,
        backgroundColor: Colors.white24,
        elevation: 0,
        onPressed: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => const SearchScreen()));
        },
        child: const Icon(
          Icons.search,
          color: Colors.white,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }

  Widget _buildUI() {
    if (weather == null) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/sky-with-clouds.jpg"),
              fit: BoxFit.cover)),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 15, sigmaY: 15),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.9,
          width: MediaQuery.of(context).size.width * 0.9,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.white60, Colors.white10]),
          ),
          child: ListView(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.08,
              ),
              _locationHeader(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _dateAndTime(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _weatherIcon(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _tempratureInfo(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.08,
              ),
              _extraInfo(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.08,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _locationHeader() {
    return Text(
      weather?.areaName ?? "Lorehm",
      textAlign: TextAlign.center,
      style: GoogleFonts.bebasNeue(
          fontSize: 40, fontWeight: FontWeight.w400, color: Colors.white),
    );
  }

  Widget _dateAndTime() {
    DateTime date = weather!.date!;

    return Column(
      children: [
        Text(
          DateFormat("h:mm a").format(date),
          style: GoogleFonts.cabin(
              fontSize: 30, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              DateFormat("EEE").format(date),
              style: GoogleFonts.cabin(
                  fontSize: 25,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              "  ${DateFormat("M/d/y").format(date)}",
              style: GoogleFonts.cabin(
                  fontSize: 25,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ],
    );
  }

  Widget _weatherIcon() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          weather!.weatherDescription!,
          style: GoogleFonts.cabin(
              fontSize: 25, color: Colors.white, fontWeight: FontWeight.w400),
        ),
        const SizedBox(
          width: 5,
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.20,
          width: MediaQuery.of(context).size.width * 0.20,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      "https://openweathermap.org/img/wn/${weather?.weatherIcon}@2x.png"))),
        )
      ],
    );
  }

  Widget _tempratureInfo() {
    return Text(
      "${weather?.temperature?.celsius?.toStringAsFixed(0)}°C",
      textAlign: TextAlign.center,
      style: GoogleFonts.cabin(
          fontSize: 40, color: Colors.white, fontWeight: FontWeight.w400),
    );
  }

  Widget _extraInfo() {
    return Container(
      height: MediaQuery.sizeOf(context).height * 0.15,
      width: MediaQuery.sizeOf(context).width * 0.80,
      decoration: BoxDecoration(
        color: Colors.white12,
        borderRadius: BorderRadius.circular(
          20,
        ),
      ),
      padding: const EdgeInsets.all(
        8.0,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Max: ${weather?.tempMax?.celsius?.toStringAsFixed(0)}° C",
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              ),
              Text(
                "Min: ${weather?.tempMin?.celsius?.toStringAsFixed(0)}° C",
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Wind: ${weather?.windSpeed?.toStringAsFixed(0)}m/s",
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              ),
              Text(
                "Humidity: ${weather?.humidity?.toStringAsFixed(0)}%",
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              )
            ],
          )
        ],
      ),
    );
  }
}

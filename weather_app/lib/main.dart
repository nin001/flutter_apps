import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather/weather.dart';
import 'package:weather_app/consts.dart';
import 'package:weather_app/controller/state_provider.dart';
import 'package:weather_app/view/homescreen.dart';

void main() {
  runApp(const MainApp());
}

WeatherFactory weatherFactory = WeatherFactory(WEATHER_API_KEY);

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return CityProvider(city: "Pune");
      },
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
    );
  }
}
